package edu.ulyanova.nstu.networkprotocols;


import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * Binary tree array based implementation.
 * Binary tree is stored in array. If the tree is a complete binary tree, this method wastes no space.
 * In this arrangement, if a node has an index i, its children are found at indices  2i+1 (for the left child)
 * and 2i+2 (for the right). Its parent (if any) is found at index (i-1)/2, assuming the root has index zero.
 *
 * Elements are supposed to implement {@link Comparable}.
 * Initial size of array is 15 elements, 3 levels.
 * The root element is on the zero level, the children of the root element are on the first level and so on.
 */
public class ArrayBasedBinaryTree<T extends Comparable<T>> implements BinaryTree<T> {
	private static Logger LOGGER = Logger.getLogger("ArrayBasedBinaryTree");
	private int maxLevel = 3;
	private int maxElementsAmount = 15; // 2^0+2^1+2^2+2^3=15
	private int elementsAmount = 0;
	private T[] elements;

	public ArrayBasedBinaryTree(Class<T> type) {
		elements = (T[]) Array.newInstance(type, maxElementsAmount);
	}

	@Override
	public void insert(T element) {
		if (isEmpty()) {
			elements[0] = element;
			elementsAmount++;
			return;
		}

		int index = 0;
		while (elements[index] != null) {
			int compareResult = element.compareTo(elements[index]);
			if(compareResult < 0){
				index = getLeftChildIndex(index);
			} else {
				index = getRightChildIndex(index);
			}
			reallocateIfOutOfBoundaries(index);
		}
		elements[index] =  element;
		elementsAmount++;
	}

	private void reallocateIfOutOfBoundaries(int index) {
		if (index <= maxElementsAmount) {
			return;
		}
		LOGGER.info("Going to reallocate the array: maxLevel:" + maxLevel + ", size:" + maxElementsAmount);
		maxLevel++;
		maxElementsAmount = maxElementsAmount + (int) Math.pow(2, maxLevel);
		elements = Arrays.copyOf(elements, maxElementsAmount);
		LOGGER.info("Finish reallocation: maxLevel:" + maxLevel + ", size:" + maxElementsAmount);
	}


	@Override
	public void deleteByIndex(int index) {

	}

	@Override
	public T searchByIndex(int searchIndex) {
		MutablePair<Integer, T> result = new MutablePair<>();
		final AtomicInteger currentLogicalNumber = new AtomicInteger(0);
		int startIndex = 0;
		Predicate<Pair<Integer, T>> isEqualToSearchIndex = pair -> pair.getLeft() == searchIndex;

		result = search(isEqualToSearchIndex, startIndex, currentLogicalNumber);
		return result !=null ? result.getRight() : null;
	}

	@Override
	public T search(T element) {
		//todo
		return null;
	}

	@Override
	public List<T> traverseInOrder() {
		if(isEmpty()) return Collections.emptyList();
		final AtomicInteger logicalNumber = new AtomicInteger(0);
		List<T> resultList =  new ArrayList<>();
		scan(0, 0, logicalNumber, resultList);
		return resultList;
	}

	private void scan(int index, int level, AtomicInteger logicalNumber, List<T> resultList) {
		if (index >= maxElementsAmount || elements[index] == null) return;
		scan(getLeftChildIndex(index), level + 1, logicalNumber, resultList);
		logicalNumber.incrementAndGet();
		resultList.add(elements[index]);
		scan(getRightChildIndex(index), level + 1, logicalNumber, resultList);
	}
//https://stackoverflow.com/questions/10117136/traversing-a-binary-tree-recursively
	private MutablePair<Integer, T>  search(Predicate<Pair<Integer, T>> predicate, int index, AtomicInteger currentLogicalNumber) {
		if (index >= maxElementsAmount || elements[index] == null) {
			return null;
		}
		MutablePair<Integer, T> resultLeft = search(predicate, getLeftChildIndex(index), currentLogicalNumber);
		if(resultLeft != null) return resultLeft;
		int logicalNumber = currentLogicalNumber.getAndIncrement();
		if (predicate.test(Pair.of(logicalNumber, elements[index]))) {
			return MutablePair.of(logicalNumber, elements[index]);
		}

		return search(predicate, getRightChildIndex(index), currentLogicalNumber);
	}

	@Override
	public boolean isEmpty() {
		return elementsAmount == 0;
	}

	private int getRightChildIndex(int index) {
		return 2 * index + 2;
	}
	private int getLeftChildIndex(int index) {
		return 2 * index + 1;
	}
}
