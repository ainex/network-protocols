package edu.ulyanova.nstu.networkprotocols;

import java.util.List;

/**
 * Binary tree interface
 */
public interface BinaryTree<T extends Comparable<T>> {

	/**
	 * Insert element into the suitable node in the binary tree
	 * @param element
	 */
	void insert(T element);

	/**
	 * Traverse the tree, count the elements and return elements with given number (index).
	 * Throws
	 * @param index
	 * @return
	 */
	T searchByIndex(int index) throws IllegalArgumentException;

	/**
	 * Search element by key, comparing it with other nodes
	 * @param element
	 * @return element or  null, if not found
	 */
	T search(T element);

	void deleteByIndex(int index)throws IllegalArgumentException;

	/**
	 * The infix traverse of the tree (LNR):
	 * 1) Check if the current node is empty or null.
	 * 2) Traverse the left subtree by recursively calling the in-order function.
	 * 3) Display the data part of the root (or current node).
	 * 4) Traverse the right subtree by recursively calling the in-order function.
	 *
	 * In a binary search tree, in-order traversal retrieves data in sorted order
	 */
	List<T> traverseInOrder();

	/**
	 * Check if tree is empty
	 * @return true or false
	 */
	boolean isEmpty();



	/*Набор операций для
упорядоченной - добавление, удаление по индексу, ускоренный поиск по ключу и
индексу, вставка с сохранением порядка*/
}
