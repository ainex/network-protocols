package edu.ulyanova.nstu.networkprotocols;

import java.util.List;
import java.util.UUID;

/**
 * Hello world!
 */
public class App {
	public static void main(String[] args) {

		BinaryTree<String> tree = new ArrayBasedBinaryTree<>(String.class);
		tree.insert("Hi");
		tree.insert("Nice");
		tree.insert("To");
		tree.insert("Meet");
		tree.insert("You");

		for (int i = 0; i < 10; i++) {
			tree.insert(UUID.randomUUID().toString());
		}
		List<String> result = tree.traverseInOrder();
		result.forEach(System.out::println);

		String search = tree.searchByIndex(3);
		System.out.println("search by index:" + search);

//		search = tree.search("Nice");
//		System.out.println("search by element:" + search);
	}
}
