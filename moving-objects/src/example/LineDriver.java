package example;

import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public interface LineDriver {
    public void open(String ip, int port, boolean server, LineAnswer back);   
    public void close();
    public void sendData(byte data[]) throws IOException;
    public byte []recData() throws IOException;
}
