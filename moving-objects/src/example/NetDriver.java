package example;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public interface NetDriver {
    public void mesClear();
    public void mesGetSize();
    public void mesSendSize(int size);
    public void mesGetObject(int idx);
    public void mesRemObject(int idx);
    public void mesPutObject(GrObject pp);
    public void mesGetObjectList();
    public void mesPutObjectList(ArrayList<String> list);
    public void open(String ip, int port, boolean server, LineDriver line, NetAnswer back);
    public void close();
}
