package example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TCPDriver implements LineDriver{
    private Socket sk=null;
    private ServerSocket srv=null; 
    private DataInputStream in=null;
    private DataOutputStream out=null;
    @Override
    public void open(String ip, int port, boolean server,final LineAnswer back){
        try {
        if (server){
            srv = new ServerSocket(port);
            new Thread(()->{                
                try {
                // �������� ���������� � ������
                sk = srv.accept();
                in = new DataInputStream(sk.getInputStream());
                out= new DataOutputStream(sk.getOutputStream());
                Utis.synch(()->{ back.isOpen(); });
                } catch (IOException ex){    // ��������� � ����� GUI
                        Utis.synch(()->{ back.onError(ex); });
                        }
                    }).start();
            }
        else{
            sk = new Socket(ip,port);
            in = new DataInputStream(sk.getInputStream());
            out= new DataOutputStream(sk.getOutputStream());
            back.isOpen();
            }
        } catch(IOException ee){ back.onError(ee);}

    }

    @Override
    public void close() {
        try {
            sk.close();
            } catch (IOException ex) {}
    }

    @Override
    public void sendData(byte[] data) throws IOException {
        out.writeInt(data.length);
        out.write(data);
        }

    @Override
    public byte[] recData() throws IOException {
        int sz = in.readInt();
        byte bb[]= new byte[sz];
        in.read(bb);
        return bb;
    }
    
}
