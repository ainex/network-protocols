package example;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TCPConnect implements NetDriver{
    private NetAnswer back=null;
    private LineDriver line=null;
    public final static int mClear=1;
    public final static int mSendSize=2;
    public final static int mGetSize=3;
    public final static int mGetObject=4;
    public final static int mRemObject=5;
    public final static int mPutObject=6;
    public final static int mGetObjectList=7;
    public final static int mPutObjectList=8;
    private boolean shutdown=false;
    Runnable loop = new Runnable(){         // ��������
        @Override
        public void run() {
            while(!shutdown){
                try {
                    InData in = new InData(line.recData());
                    int code = in.readInt();
                    switch(code){
                        case mClear:    
                            Utis.synch(()->{ back.mesClear(); });
                            break;
                        case mSendSize:    
                            final int v1 = in.readInt();
                            Utis.synch(()->{ back.mesSendSize(v1); });
                            break;
                        case mGetSize:    
                            Utis.synch(()->{ back.mesGetSize(); });
                            break;
                        case mGetObject:    
                            final int v2 = in.readInt();
                            Utis.synch(()->{ back.mesGetObject(v2); });
                            break;
                        case mRemObject:    
                            final int v3 = in.readInt();
                            Utis.synch(()->{ back.mesRemObject(v3); });
                            break;
                        case mPutObject:    
                            final String ss = in.readUTF();
                            try {
                                Class cc=null;
                                cc = Class.forName(ss);
                                final GrObject pp=(GrObject)cc.newInstance();
                                pp.Load(in);                            
                                Utis.synch(()->{ back.mesPutObject(pp); });
                                } 
                                // ���� �� � ������� ������ ����� � ����������
                                // ������� - ��� ������ - ����� ������� - ������ ����
                                // ������ ����� ������ ���������
                                catch (ClassNotFoundException ex) {} 
                                catch (InstantiationException ex) {} 
                                catch (IllegalAccessException ex) {}
                            break;
                        }
                    //----- ������������� ����� ����������, ���� �� ����������� ��� ��������� - reset
                    } catch (IOException ex) { Utis.synch(()->{ back.onError(ex); });}
                }
            }
        };
    public void mesClear() {
        try {    
            OutData out = new OutData();
            out.writeInt(mClear);
            line.sendData(out.create());
            } catch (IOException ex) { back.onError(ex);}
        }
    public void mesGetSize() {
        try {
            OutData out = new OutData();
            out.writeInt(mGetSize);
            line.sendData(out.create());
            } catch (IOException ex) { back.onError(ex);}
        }
    public void mesSendSize(int size) {
        try {
            OutData out = new OutData();
            out.writeInt(mSendSize);
            out.writeInt(size);
            line.sendData(out.create());
            } catch (IOException ex) { back.onError(ex);}
        }
    public void mesGetObject(int idx) {
        try {
            OutData out = new OutData();
            out.writeInt(mGetObject);
            out.writeInt(idx);
            line.sendData(out.create());
            } catch (IOException ex) { back.onError(ex);}
        }
    public void mesRemObject(int idx) {
        try {
            OutData out = new OutData();
            out.writeInt(mRemObject);
            out.writeInt(idx);
            line.sendData(out.create());
            } catch (IOException ex) { back.onError(ex);}
        }
    public void mesPutObject(GrObject pp) {
        try {
            OutData out = new OutData();
            out.writeInt(mPutObject);
            out.writeUTF(pp.getClass().getName());
            pp.Save(out);
            line.sendData(out.create());
            } catch (IOException ex) { back.onError(ex);}
        }
    public void open(String ip, int port, boolean server, LineDriver line0, NetAnswer back0) {
        back = back0;
        line = line0;
        line.open(ip, port, server, new LineAnswer(){
            @Override
            public void isOpen() {
                new Thread(loop).start();
                back.isOpen();
                }
            @Override
            public void onError(Exception ee) {
                back.onError(ee);
                }
            });
        }
    public void close() {
        shutdown=true;
        line.close();
        back.isClosed();
        }
    //---------------------------------------------------------------------------
    public static void main(String argv[]){
        NetAnswer answer = new NetAnswer(){
            @Override
            public void mesClear() {
                System.out.println("clear");
                }
            @Override
            public void mesGetSize() {
                System.out.println("getSize");
                }
            @Override
            public void mesSendSize(int size) {
                System.out.println("size="+size);
                }
            @Override
            public void mesGetObject(int idx) {
                System.out.println("get="+idx);
                }
            @Override
            public void mesRemObject(int idx) {
                System.out.println("rem="+idx);
                }
            @Override
            public void mesPutObject(GrObject pp) {
                System.out.println("put="+pp);
                }
            @Override
            public void isOpen() {
                System.out.println("open");
                }
            @Override
            public void isClosed() {
                System.out.println("closed");
                }
            @Override
            public void onError(Exception ee) {
                System.out.println("error="+ee.getMessage());
                }            
            @Override
            public void mesGetObjectList() {
                }
            @Override
            public void mesPutObjectList(ArrayList<String> list) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };
        TCPConnect s1 = new TCPConnect();
        s1.open("localhost",7700,true,new UDPDriver(),answer);
        TCPConnect s2 = new TCPConnect();
        s2.open("localhost",7700,false,new UDPDriver(),answer);
        s2.mesPutObject(new Circle(2,3,4,null));
        s2.mesSendSize(124);
        new Thread(()->{
            try {
                Thread.sleep(5000);
                Utis.synch(()->{
                    s1.close();
                    s2.close();
                    });
                } catch (InterruptedException ex) {}
            }).start();
        }
    @Override
    public void mesGetObjectList() {
        }
    @Override
    public void mesPutObjectList(ArrayList<String> list) {
        }
    
}
