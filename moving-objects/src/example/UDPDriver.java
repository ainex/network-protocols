package example;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class UDPDriver implements LineDriver{
    private DatagramSocket send=null;
    private String ip;
    private int sndPort;
    @Override
    public void close() {
        send.close();
        }

    @Override
    public void sendData(byte[] data) throws IOException {
        DatagramPacket packet = new DatagramPacket(data, data.length,new InetSocketAddress(ip, sndPort));
        send.send(packet);
        }

    @Override
    public byte[] recData() throws IOException {
        byte[] buffer = new byte[512];                   // ����� ��� �������� ������
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length); // ������� ����������� �����
        send.receive(packet);                          // ��������� ������, ����������� �����.
        return packet.getData();                       // �������� � ����� �����, �� ��������� ������
        }

    @Override
    public void open(String ip0, int port, boolean server, LineAnswer back) {
        try {
            ip = ip0;
            sndPort = server ? port : port+1;
            send = new DatagramSocket(server ? port+1 : port);
            back.isOpen();
            } catch (SocketException ex) { back.onError(ex); }
        }
    
}
