package example;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class OutData extends DataOutputStream{   
    public OutData() {
        super(new ByteArrayOutputStream());
        }
    public byte[] create() throws IOException{
        ByteArrayOutputStream xx = (ByteArrayOutputStream)super.out;
        xx.flush();
        return xx.toByteArray();
        }  
    public void reUse(){
        super.out = new ByteArrayOutputStream();
        }    
}
