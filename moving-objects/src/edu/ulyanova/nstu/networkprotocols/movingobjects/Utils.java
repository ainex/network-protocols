package edu.ulyanova.nstu.networkprotocols.movingobjects;

public class Utils {
    private Utils() {
    }

    public static void synch(Runnable code) {
        java.awt.EventQueue.invokeLater(code);
    }
}
