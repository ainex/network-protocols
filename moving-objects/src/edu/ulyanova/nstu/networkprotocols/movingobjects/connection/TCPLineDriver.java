package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

import edu.ulyanova.nstu.networkprotocols.movingobjects.Utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPLineDriver implements LineDriver {
    private Socket sk;
    private ServerSocket srv;
    private DataInputStream in;
    private DataOutputStream out;

    @Override
    public void open(String ip, int port, boolean server, final LineCallback back) {
        try {
            if (server) {
                srv = new ServerSocket(port);
                new Thread(() -> {
                    try {

                        sk = srv.accept();
                        in = new DataInputStream(sk.getInputStream());
                        out = new DataOutputStream(sk.getOutputStream());
                        Utils.synch(() -> {
                            back.onOpen();
                        });
                    } catch (IOException ex) {
                        Utils.synch(() -> {
                            back.onError(ex);
                        });
                    }
                }).start();
            } else {
                sk = new Socket(ip, port);
                in = new DataInputStream(sk.getInputStream());
                out = new DataOutputStream(sk.getOutputStream());
                back.onOpen();
            }
        } catch (IOException ee) {
            back.onError(ee);
        }

    }

    @Override
    public void close() {
        try {
            if (sk != null) {
                sk.close();
            }
            if (srv != null) {
                srv.close();
            }
        } catch (IOException ex) {
        }
    }

    @Override
    public void sendData(byte[] data) throws IOException {
        out.writeInt(data.length);
        out.write(data);
    }

    @Override
    public byte[] receiveData() throws IOException {
        int sz = in.readInt();
        byte bb[] = new byte[sz];
        in.read(bb);
        return bb;
    }

}
