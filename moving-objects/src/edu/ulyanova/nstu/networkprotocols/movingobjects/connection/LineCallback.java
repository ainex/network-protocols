package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

public interface LineCallback {
    void onOpen();
    void onError(Exception ee);
}
