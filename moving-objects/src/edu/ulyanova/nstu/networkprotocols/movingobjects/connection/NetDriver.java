package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

import edu.ulyanova.nstu.networkprotocols.movingobjects.commands.Command;
import edu.ulyanova.nstu.networkprotocols.movingobjects.commands.Context;


/**
 * Интерфейс для протокола взаимодействия между двумя приложениями
 */
public interface NetDriver {

    void sendCommand(Command command);

    void start(boolean isServerMode, LineDriver line, Context context);

    void stop();
}
