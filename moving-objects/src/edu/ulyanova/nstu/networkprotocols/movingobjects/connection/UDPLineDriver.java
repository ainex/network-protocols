package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;


public class UDPLineDriver implements LineDriver {
    private static final int BUFFER_SIZE = 512*512;
    private DatagramSocket socket = null;
    private String ip;
    private int sendPort;

    @Override
    public void close() {
        if (null != socket) {
            socket.close();
        }
    }

    @Override
    public void sendData(byte[] data) throws IOException {
        DatagramPacket packet = new DatagramPacket(data, data.length, new InetSocketAddress(ip, sendPort));
        socket.send(packet);
    }

    @Override
    public byte[] receiveData() throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        socket.receive(packet);
        return packet.getData();
    }

    @Override
    public void open(String ip, int port, boolean server, LineCallback back) {
        try {
            this.ip = ip;
            sendPort = server ? port : port + 1;
            socket = new DatagramSocket(server ? port + 1 : port);
            back.onOpen();
        } catch (SocketException ex) {
            back.onError(ex);
        }
    }

}
