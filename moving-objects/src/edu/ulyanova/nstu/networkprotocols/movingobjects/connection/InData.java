package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

public class InData {
    private ByteArrayInputStream bis = null;
    private ObjectInput in = null;

    public InData(byte[] in) {
        bis = new ByteArrayInputStream(in);
    }

    public Object readObject() throws IOException, ClassNotFoundException {
        in = new ObjectInputStream(bis);
        Object o = in.readObject();
        //in.close();
        return o;
    }
}
