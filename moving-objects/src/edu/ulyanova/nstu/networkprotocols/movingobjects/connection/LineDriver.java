package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;


import java.io.IOException;

/**
 *
 * Драйвер для передачи по протоколу UPD или TCP
 */
public interface LineDriver {
    void open(String ip, int port, boolean server, LineCallback back);
    void close();
    void sendData(byte[] data) throws IOException;
    byte [] receiveData() throws IOException;
}
