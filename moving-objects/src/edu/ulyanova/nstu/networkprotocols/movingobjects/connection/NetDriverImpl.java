package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

import edu.ulyanova.nstu.networkprotocols.movingobjects.commands.Command;
import edu.ulyanova.nstu.networkprotocols.movingobjects.commands.Context;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class NetDriverImpl implements NetDriver {
    private static final int SERVER_PORT = 9000;
    private static final String HOST = "localhost";

    private LineDriver line;
    private Context context;
    private final AtomicBoolean running = new AtomicBoolean(false);


    private Runnable loop = new Runnable() {
        @Override
        public void run() {
            running.set(true);
            while (running.get()) {
                try {
                    InData in = new InData(line.receiveData());
                    System.out.println("Blocked on reading from socket...");
                    final Command command = (Command) in.readObject();
                    System.out.println("Got command: " + command.getType());
                    new Thread(() -> {
                        Command resultCommand = command.execute(context);
                        if (!resultCommand.isNoReply()) {
                            sendCommand(resultCommand);
                        }
                    }).start();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
    };


    public void start(boolean isServerMode, LineDriver line, Context context) {
        this.line = line;
        this.context = context;

        line.open(HOST, SERVER_PORT, isServerMode, new LineCallback() {
            @Override
            public void onOpen() {
                new Thread(loop).start();
                //back.onOpen();
            }

            @Override
            public void onError(Exception ee) {
                System.out.println(ee);
            }
        });

    }

    @Override
    public void stop() {
        running.set(false);
        line.close();
        System.out.println("thread is going to be stopped...");
    }

    @Override
    public synchronized void sendCommand(Command command) {
        if (command == null) return;
        try {
            OutData outData = new OutData();
            outData.writeObject(command);
            line.sendData(outData.create());
            System.out.println("send command: " + command.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
