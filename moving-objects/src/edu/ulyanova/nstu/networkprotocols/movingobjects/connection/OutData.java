package edu.ulyanova.nstu.networkprotocols.movingobjects.connection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author User
 */
public class OutData {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    public byte[] create() throws IOException {
        bos.flush();

        return bos.toByteArray();
    }


    public void writeObject(Object o) throws IOException {
        ObjectOutput out = null;

        out = new ObjectOutputStream(bos);

        out.writeObject(o);
        out.flush();
    }

    public static void main(String[] args) {
        OutData outData = new OutData();
        try {
            outData.writeObject("Hi there");


            InData inData = new InData(outData.create());
            String result = (String) inData.readObject();
            System.out.println("Awesome transformation: " + result);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
