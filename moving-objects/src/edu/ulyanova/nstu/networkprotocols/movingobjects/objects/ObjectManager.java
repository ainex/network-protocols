package edu.ulyanova.nstu.networkprotocols.movingobjects.objects;

import java.util.LinkedList;
import java.util.List;

public class ObjectManager {
    private List<BaseObject> objects = new LinkedList<>();

    public void clear() {
        objects.stream().forEach(grObject -> grObject.stop = true);
        objects.clear();
        System.out.println("Objects are cleared");
    }

    public void add(BaseObject object) {
        objects.add(object);
    }

    public BaseObject get(int index) {
        return objects.get(index);
    }

    public void remove(int index) {
        if (index < objects.size()) {
            BaseObject object = objects.remove(index);
            object.stop = true;
        }
    }

    public int size() {
        return objects.size();
    }

    public List<BaseObject> getAll() {
        return new LinkedList<>(objects);
    }

}
