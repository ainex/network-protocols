package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

public class DeleteObjectCommand extends Command {
    private int index;

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public Commands getType() {
        return Commands.DELETE_OBJECT;
    }


    @Override
    public Command execute(Context context) {
        if (index < context.getObjectManager().size()) {
            context.getObjectManager().remove(index);
        }
        return new NoReplyCommand();
    }
}
