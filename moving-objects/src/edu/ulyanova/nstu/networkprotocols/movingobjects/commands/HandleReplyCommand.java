package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

import edu.ulyanova.nstu.networkprotocols.movingobjects.objects.BaseObject;
import edu.ulyanova.nstu.networkprotocols.movingobjects.objects.ObjectManager;

import javax.swing.*;
import java.util.List;

public class HandleReplyCommand extends Command {
    private Commands replyToType;
    private Object reply;

    public HandleReplyCommand(Commands replyToType, Object reply) {
        this.replyToType = replyToType;
        this.reply = reply;
    }

    @Override
    public Commands getType() {
        return Commands.HANDLE_REPLY;
    }

    @Override
    public Command execute(Context context) {
        System.out.println("received reply type: " + replyToType);
        ObjectManager objectManager = context.getObjectManager();
        switch (replyToType) {
            case GET_OBJECTS:
                objectManager.clear();
                List<BaseObject> newObjects = (List<BaseObject>) reply;
                newObjects.stream().forEach(o -> {
                    o.setComponent(context.getTXT());
                    objectManager.add(o);
                    new Thread(o).start();
                });
                break;
            case GET_OBJECTS_SIZE:
                context.getTT().setText(reply.toString());
                break;
            case GET_OBJECT:
                BaseObject baseObject = (BaseObject) reply;
                baseObject.setComponent(context.getTXT());
                objectManager.add(baseObject);
                new Thread(baseObject).start();
                break;
            case GET_OBJECTS_LIST:
                JComboBox objectList = context.getObjectList();
                objectList.removeAllItems();
                List<String> resultObjects = (List) reply;
                if (!resultObjects.isEmpty()) {
                    resultObjects.forEach(e -> objectList.addItem(e));
                }
                objectList.setSelectedIndex(-1);
                break;
            case NO_REPLY:
            default:

        }
        return new NoReplyCommand();
    }
}
