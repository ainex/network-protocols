package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

public class ClearObjectsCommand extends Command {
    @Override
    public Commands getType() {
        return Commands.CLEAR_OBJECTS;
    }

    @Override
    public Command execute(Context context) {
        context.getObjectManager().clear();
        return new NoReplyCommand();
    }
}
