package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

import edu.ulyanova.nstu.networkprotocols.movingobjects.objects.BaseObject;

import java.util.List;
import java.util.stream.Collectors;

public class GetObjectsListCommand extends Command {

    @Override
    public Commands getType() {
        return Commands.GET_OBJECTS_LIST;
    }


    @Override
    public Command execute(Context context) {
        List<String> result = context.getObjectManager().getAll()
                .stream()
                .map(BaseObject::toString)
                .collect(Collectors.toList());
        return new HandleReplyCommand(getType(), result);
    }
}
