package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

import java.io.Serializable;

public abstract class Command implements Serializable {
    public abstract Commands getType();

    public boolean isNoReply() {
        return getType() == Commands.NO_REPLY;
    }

    public abstract Command execute(Context context);

}
