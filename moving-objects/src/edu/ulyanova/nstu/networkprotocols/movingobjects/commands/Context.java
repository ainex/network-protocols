package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

import edu.ulyanova.nstu.networkprotocols.movingobjects.objects.ObjectManager;

import javax.swing.*;

public class Context {
    private final ObjectManager objectManager;
    private final JTextField TT;
    private final JTextArea TXT;
    private final JComboBox objectList;

    public Context(ObjectManager objectManager, JTextField TT, JTextArea TXT, JComboBox objectList) {
        this.objectManager = objectManager;
        this.TT = TT;
        this.TXT = TXT;
        this.objectList = objectList;
    }

    public ObjectManager getObjectManager() {
        return objectManager;
    }

    public JTextField getTT() {
        return TT;
    }

    public JTextArea getTXT() {
        return TXT;
    }

    public JComboBox getObjectList() {
        return objectList;
    }
}
