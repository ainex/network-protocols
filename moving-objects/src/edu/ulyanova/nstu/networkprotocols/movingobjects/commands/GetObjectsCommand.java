package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

public class GetObjectsCommand extends Command {
    @Override
    public Commands getType() {
        return Commands.GET_OBJECTS;
    }

    @Override
    public Command execute(Context context) {
        return new HandleReplyCommand(getType(), context.getObjectManager().getAll());
    }
}
