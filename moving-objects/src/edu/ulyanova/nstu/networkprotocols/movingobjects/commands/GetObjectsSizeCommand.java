package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

public class GetObjectsSizeCommand extends Command {
    @Override
    public Commands getType() {
        return Commands.GET_OBJECTS_SIZE;
    }

    @Override
    public Command execute(Context context) {
        return new HandleReplyCommand(getType(), context.getObjectManager().size());
    }
}
