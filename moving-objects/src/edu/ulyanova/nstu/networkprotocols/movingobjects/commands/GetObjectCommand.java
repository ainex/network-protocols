package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

import edu.ulyanova.nstu.networkprotocols.movingobjects.objects.BaseObject;

public class GetObjectCommand extends Command {
    private int index;

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public Commands getType() {
        return Commands.GET_OBJECT;
    }


    @Override
    public Command execute(Context context) {
        BaseObject result = context.getObjectManager().size() >= index ? context.getObjectManager().get(index) : null;
        return new HandleReplyCommand(getType(), result);
    }
}
