package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

public class NoReplyCommand extends Command {
    @Override
    public Commands getType() {
        return Commands.NO_REPLY;
    }

    @Override
    public Command execute(Context context) {
        return new NoReplyCommand();
    }

}
