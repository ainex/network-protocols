package edu.ulyanova.nstu.networkprotocols.movingobjects.commands;

public enum Commands {
    NO_REPLY,
    CLEAR_OBJECTS,
    ADD_OBJECT,
    GET_OBJECTS_SIZE,
    GET_OBJECT,
    DELETE_OBJECT,
    GET_OBJECTS,
    GET_OBJECTS_LIST,
    HANDLE_REPLY;
}
